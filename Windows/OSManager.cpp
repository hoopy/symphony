#include "OSManager.h"

#include <process.h>
#include <GL/glew.h>
#include <GL/wglew.h>
#include <iostream>

extern "C" IMAGE_DOS_HEADER __ImageBase;
HINSTANCE hInstance = (HINSTANCE)&__ImageBase;

void EnableOpenGLWindow(HWND hwnd, HDC* hDC, HGLRC* hRC);
void DisableOpenGLWindow(HWND hwnd, HDC hDC, HGLRC hRC);

void EnableOpenGLWindow(HWND hwnd, HDC* hDC, HGLRC* hRC)
{
    PIXELFORMATDESCRIPTOR pfd;
    int iFormat;
    /* get the device context (DC) */
    *hDC = GetDC(hwnd);
    /* set the pixel format for the DC */
    ZeroMemory(&pfd, sizeof(pfd));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW |
                  PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cDepthBits = 32;
    pfd.iLayerType = PFD_MAIN_PLANE;
    iFormat = ChoosePixelFormat(*hDC, &pfd);
    SetPixelFormat(*hDC, iFormat, &pfd);
    /* create and enable the render context (RC) */

    *hRC = wglCreateContext(*hDC);
}

void DisableOpenGLWindow(HWND hwnd, HDC hDC, HGLRC hRC)
{
    wglDeleteContext(hRC);
    ReleaseDC(hwnd, hDC);
}

LRESULT CALLBACK OSManager::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_CLOSE:
            PostQuitMessage(0);
        break;

        case WM_DESTROY:
            return 0;

        case WM_KEYDOWN:
        {
            switch (wParam)
            {
                case VK_ESCAPE:
                    PostQuitMessage(0);
                break;
            }
        }
        break;

        default:
            return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }

    return 0;
}

void OSManager::SetDrawable()
{
    wglMakeCurrent(NULL, NULL);
    wglMakeCurrent(hDC, hRC);
}

OSManager::OSManager(char* Name, int Width, int Height)
{
    isReady = false;
    wName = Name;
    wWidth = Width;
    wHeight = Height;
    // clear out the window class for use
    ZeroMemory(&wc, sizeof(WNDCLASSEX));
    // fill in the struct with the needed information
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
    wc.lpszClassName = "Symphony";

    RegisterClassEx(&wc);
    // create the window and use the result as the handle

    bQuit = true;
    unsigned thr = 0;
    _beginthreadex(NULL, 0, WindowsMessageHandler, this, 0, &thr);

    while(bQuit)
    {}//Waiting for the window to be created

    EnableOpenGLWindow(hwnd, &hDC, &hRC);
    SetDrawable();

    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        std::cout << "GLEW did not initialize properly" << std::endl;
    }
    if(GLEW_ARB_shader_objects)
    {
        std::cout << "You have OpenGL Shaders" << std::endl;
    }

}

unsigned __stdcall OSManager::WindowsMessageHandler(void *pThis)
{
    OSManager* Disp = (OSManager*)pThis;
    Disp->MessageLoop();
    return 0;
}

void OSManager::WaitForReady()
{
    while(!isReady)
    {}
}

void OSManager::MessageLoop()
{
    hwnd = CreateWindowEx(NULL,
        "Symphony",    // name of the window class
        wName,   // title of the window
        WS_OVERLAPPEDWINDOW,    // window style. Always windowed for now.
        CW_USEDEFAULT,    // x-position of the window
        CW_USEDEFAULT,    // y-position of the window
        wWidth,    // width of the window
        wHeight,    // height of the window
        NULL,    // we have no parent window, NULL
        NULL,    // we aren't using menus, NULL
        hInstance,    // application handle
        NULL);
    ShowWindow(hwnd, 1);

    bQuit = false;
    isReady = true;
    while(!bQuit)
    {
        /* check for messages */
        if (PeekMessage(&msg, hwnd, 0, 0, PM_REMOVE))
        {
            /* handle or dispatch messages */
            switch(msg.message)
            {
                case WM_QUIT:
                    bQuit = true;
                    break;

                default:
                    TranslateMessage(&msg);
                    DispatchMessage(&msg);
                    break;
            }
        }
    }
    DestroyWindow(hwnd);
}

bool OSManager::isRunning()
{
    return !bQuit;
}

OSManager::~OSManager()
{
    DisableOpenGLWindow(hwnd, hDC, hRC);
}

void OSManager::Render()
{
    SetDrawable();
    SwapBuffers(hDC);
}

int OSManager::GetWidth()
{
    return wWidth;
}

int OSManager::GetHeight()
{
    return wHeight;
}
