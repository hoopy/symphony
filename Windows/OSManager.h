#ifndef OSMANAGER_H
#define OSMANAGER_H

#include "../BuildConfig.h"
#include <windows.h>

class OSManager
{
    public:
        EXPORT OSManager(char * Name, int Width, int Height);
        EXPORT ~OSManager();
        EXPORT void WaitForReady();
        void SetDrawable();
        EXPORT bool isRunning();
        EXPORT void Render();
        EXPORT int GetWidth();
        EXPORT int GetHeight();

    private:
        WNDCLASSEX wc;
        HWND hwnd;
        HDC hDC;
        HGLRC hRC;
        MSG msg;

        bool bQuit;
        bool isReady;
        char* wName;
        int wWidth, wHeight;

        static LRESULT __stdcall WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
        static unsigned __stdcall WindowsMessageHandler(void *pThis);
        void MessageLoop();

};

#endif
