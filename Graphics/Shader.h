#ifndef SHADER_H
#define SHADER_H

#include "../BuildConfig.h"

class Shader
{
    public:
        Shader(const char * VertexShader, const char * FragmentShader);
        ~Shader();
        unsigned int GetProgramID();
        unsigned int GetMatrixID();
    private:
        unsigned int program;
        unsigned int shaderMatrix;
};

#endif // SHADER_H
