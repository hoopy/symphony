#ifndef DRAWFACTORY_H
#define DRAWFACTORY_H

#include <GL/glew.h>
#include <glm.hpp>

class DrawFactory
{
    public:
        DrawFactory();
        ~DrawFactory();
        void PrimeFactory(unsigned int matrix);
        void SendFactoryPerspective(glm::mat4 perspective);
        void SendFactoryView(glm::mat4 view);
        void DrawVBO(unsigned int vboHandle, unsigned int cboHandle, unsigned int verticies, glm::mat4 M);
    private:
        glm::mat4 V;
        glm::mat4 P;
        unsigned int matrixID;
};

#endif // DRAWFACTORY_H
