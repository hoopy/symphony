#include "Mesh.h"

#include <GL/glew.h>

Mesh::Mesh()
{
    glGenBuffers(1, &vboHandle);
    glGenBuffers(1, &cboHandle);

    vertexArray[0] = -1;
    vertexArray[1] = -1;
    vertexArray[2] = 0;

    vertexArray[3] = 1;
    vertexArray[4] = -1;
    vertexArray[5] = 0;

    vertexArray[6] = 0;
    vertexArray[7] = 1;
    vertexArray[8] = 0;

    glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexArray), vertexArray, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, cboHandle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colorArray), colorArray, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

Mesh::~Mesh()
{
    glDeleteBuffers(1, &vboHandle);
    glDeleteBuffers(1, &cboHandle);
    delete(&vertexArray);
    delete(&colorArray);
}

void Mesh::Draw(DrawFactory * df, glm::mat4 M)
{
    df->DrawVBO(vboHandle, cboHandle, sizeof(vertexArray)/12, M);
}
