#include "DrawFactory.h"

#include <ext.hpp>

DrawFactory::DrawFactory()
{

}

DrawFactory::~DrawFactory()
{

}

void DrawFactory::PrimeFactory(unsigned int matrix)
{
    matrixID = matrix;
}

void DrawFactory::SendFactoryPerspective(glm::mat4 perspective)
{
    P = perspective;
}

void DrawFactory::SendFactoryView(glm::mat4 view)
{
    V = view;
}

void DrawFactory::DrawVBO(unsigned int vboHandle, unsigned int cboHandle, unsigned int verticies, glm::mat4 M)
{

    glm::mat4 MVP = P * V * M;

    glUniformMatrix4fv(matrixID, 1, GL_FALSE, &MVP[0][0]);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glBindBuffer(GL_ARRAY_BUFFER, cboHandle);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glDrawArrays(GL_TRIANGLES, 0, verticies);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDisableVertexAttribArray(0);
}
