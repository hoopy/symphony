#ifndef MESH_H
#define MESH_H

#include "../BuildConfig.h"
#include "DrawFactory.h"

class Mesh
{
    public:
        Mesh();
        virtual ~Mesh();
        void Draw(DrawFactory * df, glm::mat4 M);
    private:
        unsigned int vboHandle;
        unsigned int cboHandle;
        float vertexArray[9];
        float colorArray[9];
};

#endif // MESH_H
