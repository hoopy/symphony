#ifndef CHUNKMANAGER_H
#define CHUNKMANAGER_H

#include <vector>
#include "../BuildConfig.h"
#include "Chunk.h"
#include "../Graph/Node.h"

class ChunkManager: public Node
{
    public:
        ChunkManager();
        virtual EXPORT ~ChunkManager();
        void EXPORT Draw(DrawFactory * df);
    private:
        std::vector<Chunk*> ChunkList;
};

#endif // CHUNKMANAGER_H
