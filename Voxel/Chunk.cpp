#include "Chunk.h"

#include <GL/glew.h>

Chunk::Chunk()
{
    glGenBuffers(1, &vboHandle);
    glGenBuffers(1, &cboHandle);
    Recalculate();
}

Chunk::~Chunk()
{
    glDeleteBuffers(1, &vboHandle);
    glDeleteBuffers(1, &cboHandle);
    delete(&vertexArray);
    delete(&colorArray);
}

void Chunk::DrawChunk(DrawFactory * df)
{
    df->DrawVBO(vboHandle, cboHandle, sizeof(vertexArray)/12, glm::mat4(1.0f));
}

void Chunk::Recalculate()
{
    glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexArray), vertexArray, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, cboHandle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(colorArray), colorArray, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
