#ifndef CHUNK_H
#define CHUNK_H

#include "../BuildConfig.h"
#include "../Graphics/DrawFactory.h"

class Chunk
{
    public:
        Chunk();
        virtual ~Chunk();
        void Recalculate();
        void DrawChunk(DrawFactory * drawFactory);

    private:
        unsigned int vboHandle;
        unsigned int cboHandle;
        float vertexArray[9];
        float colorArray[9];
};

#endif // CHUNK_H
