#ifndef CAMERA_H
#define CAMERA_H

#include "../BuildConfig.h"
#include <glm.hpp>
#include <ext.hpp>

class Camera
{
    public:
        EXPORT Camera();
        EXPORT ~Camera();
        void EXPORT Translate(glm::vec3 translation);
        void EXPORT Rotate(float angle, glm::vec3 rotation);
        glm::mat4 GetViewMatrix();
        glm::mat4 GetProjectionMatrix();
    private:
        glm::mat4 viewMatrix;
        glm::mat4 projectionMatrix;
};

#endif // CAMERA_H
