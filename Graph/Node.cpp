#include "Node.h"

#include <math.h>
#include "../Util/Math.h"

Node::Node()
{
    modelMatrix = glm::mat4(1.0f);
}

Node::~Node()
{

}

void Node::BindMesh(Mesh * mesh)
{
    nodeMesh = mesh;
}

void Node::Draw(DrawFactory * df)
{
    nodeMesh->Draw(df, modelMatrix);
    DrawChildren(df);
}

void Node::DrawChildren(DrawFactory * df)
{
    for(unsigned int i=0;i<NodeList.size();i++)
    {
        NodeList.at(i)->Draw(df);
    }
}

void Node::Translate(glm::vec3 translation)
{
    modelMatrix = glm::translate(modelMatrix, translation);
}

void Node::Rotate(float angle, glm::vec3 rotation)
{
    modelMatrix = glm::rotate(modelMatrix, angle, rotation);
}
