#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include "../BuildConfig.h"
#include "Node.h"
#include "Camera.h"
#include "../Graphics/Shader.h"
#include "../Voxel/ChunkManager.h"
#include "../Graphics/DrawFactory.h"

class Scene
{
    public:
        Scene();
        EXPORT ~Scene();
        EXPORT void BindOSManager(OSManager * d);
        EXPORT void BindShader(Shader * s);
        EXPORT void BindCamera(Camera * c);

        EXPORT ChunkManager * AddChunkManager();
        EXPORT Node * AddNode();

        EXPORT void DrawScene();
        void SetViewRange(unsigned int distance);
        unsigned int GetViewRange();

    private:
        std::vector<Node*> NodeList;
        DrawFactory * drawFactory;
        OSManager * OSM;
        unsigned int ViewRange;
        Shader * shader;
        Camera * camera;
};

#endif
