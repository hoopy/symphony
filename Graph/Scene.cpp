#include "Scene.h"
#include <GL/glew.h>
#include "../Graphics/Shader.h"

Scene::Scene()
{
    drawFactory = new DrawFactory();
}

Scene::~Scene()
{
    delete(drawFactory);
}

void Scene::BindOSManager(OSManager * d)
{
    OSM = d;
    OSM->SetDrawable();
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_CULL_FACE);
}

void Scene::BindShader(Shader * s)
{
    shader = s;
}

void Scene::BindCamera(Camera * c)
{
    camera = c;
}

void Scene::SetViewRange(unsigned int distance)
{
    ViewRange = distance;
}

unsigned int Scene::GetViewRange()
{
    return ViewRange;
}

ChunkManager * Scene::AddChunkManager()
{
    ChunkManager * cm = new ChunkManager();
    NodeList.push_back(cm);
    return cm;
}

Node * Scene::AddNode()
{
    Node * n = new Node();
    NodeList.push_back(n);
    return n;
}

void Scene::DrawScene()
{

    OSM->SetDrawable();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(shader->GetProgramID());

    drawFactory->PrimeFactory(shader->GetMatrixID());
    drawFactory->SendFactoryPerspective(camera->GetProjectionMatrix());
    drawFactory->SendFactoryView(camera->GetViewMatrix());

    for(unsigned int i=0;i<NodeList.size();i++)
    {
        NodeList.at(i)->Draw(drawFactory);
    }
}
