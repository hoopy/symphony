#include "Camera.h"

#include <math.h>
#include "../Util/Math.h"

Camera::Camera()
{
    projectionMatrix = glm::mat4(1.0f);
    viewMatrix = glm::mat4(1.0f);
}

Camera::~Camera()
{

}

glm::mat4 Camera::GetViewMatrix()
{
    return viewMatrix;
}

glm::mat4 Camera::GetProjectionMatrix()
{
    return projectionMatrix;
}

void Camera::Translate(glm::vec3 translation)
{
    viewMatrix = glm::translate(viewMatrix, translation);
}

void Camera::Rotate(float angle, glm::vec3 rotation)
{
    viewMatrix = glm::rotate(viewMatrix, angle, rotation);
}
