#ifndef NODE_H
#define NODE_H

#include <vector>
#include "../BuildConfig.h"
#include "../Graphics/Mesh.h"
#include "../Graphics/DrawFactory.h"
#include <glm.hpp>
#include <ext.hpp>

class Node
{
    public:
        Node();
        virtual EXPORT ~Node();
        virtual void Draw(DrawFactory * df);
        void EXPORT BindMesh(Mesh * mesh);
        void EXPORT Translate(glm::vec3 translation);
        void EXPORT Rotate(float angle, glm::vec3 rotation);
    private:
        void DrawChildren(DrawFactory * df);
        std::vector<Node*> NodeList;
        glm::mat4 modelMatrix;
        Mesh * nodeMesh;
};

#endif
