#ifndef OSMANAGER_H
#define OSMANAGER_H

#include "../BuildConfig.h"

class OSManager
{
    public:
        EXPORT OSManager(char* Name, int Width, int Height);
        EXPORT ~OSManager();
        EXPORT void WaitForReady();
        void SetDrawable();
        EXPORT bool isRunning();
        EXPORT void Render();
        EXPORT int GetWidth();
        EXPORT int GetHeight();

    private:
        bool bQuit;
        bool isReady;
        char* wName;
        int wWidth, wHeight;

};

#endif
