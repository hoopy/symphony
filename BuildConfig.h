
#ifdef __WIN32

    #ifdef BUILD_SHARED
        #define EXPORT __declspec(dllexport)
    #else
        #define EXPORT __declspec(dllimport)
    #endif

    #include "./Windows/OSManager.h"

#endif
#ifdef __linux__

    #ifdef BUILD_SHARED
        #define EXPORT __attribute__((visibility("default")))
    #else
        #define EXPORT __attribute__((visibility("hidden")))
    #endif

    #include "./Linux/OSManager.h"

#endif
