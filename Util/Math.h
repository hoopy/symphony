#ifndef MATH_H_INCLUDED
#define MATH_H_INCLUDED

#include "../BuildConfig.h"

float EXPORT ToRadians(float degrees);

#endif // MATH_H_INCLUDED
