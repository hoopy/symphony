#include "Math.h"

#include <math.h>

float PI = atan(1)*4;

float ToRadians(float degrees)
{
    return (PI * degrees)/180;
}
