#ifndef __MAIN_H__
#define __MAIN_H__

#include "./BuildConfig.h"
#include "./Graph/Scene.h"
#include "./Graph/Camera.h"
#include "./Graph/Node.h"
#include "./Graphics/Mesh.h"
#include "./Graphics/Shader.h"

EXPORT OSManager * CreateOSManager(char* Name, int Width, int Height);
EXPORT Scene * CreateScene();
EXPORT Shader * CreateShader(const char* PathToVertex, const char* PathToFragment);
EXPORT Camera * CreateCamera();
EXPORT Mesh * CreateMesh();

#endif
