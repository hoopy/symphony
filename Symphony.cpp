#include "Symphony.h"
#include <GL/glew.h>

OSManager * CreateOSManager(char* Name, int Width, int Height)
{
    return new OSManager(Name, Width, Height);
}

Scene * CreateScene()
{
    return new Scene();
}

Shader * CreateShader(const char* PathToVertex, const char* PathToFragment)
{
    return new Shader(PathToVertex,PathToFragment);
}

Camera * CreateCamera()
{
    return new Camera();
}

Mesh * CreateMesh()
{
    return new Mesh();
}
